import React from "react";
import "./app.css";
import "antd/dist/antd.css";
import { Layout } from "antd";
import { Provider } from "react-redux";
import { LayoutMain } from "./common";
import store from "./store/store";

function App() {
  return (
    <Provider store={store}>
      <Layout className="layout" style={{ minHeight: "100vh" }}>
        <LayoutMain />
      </Layout>
    </Provider>
  );
}

export default App;
