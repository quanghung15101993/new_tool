import React from "react";
import "antd/dist/antd.css";
import { Upload, Modal } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import axios from "axios"
import {Table} from "antd";
function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

class CMT extends React.Component {
    
constructor(props) {
    super(props)
    this.state = {
    previewVisible: false,
    previewImage: '',
    previewTitle: '',
    fileList: [],
    respone: {},
    };
  }
  handleCancel = () => this.setState({ previewVisible: false });

  handlePreview = async file => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }

    this.setState({
      previewImage: file.url || file.preview,
      previewVisible: true,
      previewTitle: file.name || file.url.substring(file.url.lastIndexOf('/') + 1),
    });
  };

    handleChange = ({ fileList }) => this.setState({fileList });
  

   postImage =  async (fileList) => {
       try {
         await axios.post('http://10.7.16.122:5000/ai/ocr/v1.0/get-cic-content', fileList).then((response) => {
          this.setState({
              respone: response.data
          });
  })    
  } catch (error) {
    console.log(error);
     }
};

    submitImage(fileList)  {  
        fileList.map((file) => {
           var bodyimage = {
                image: file.thumbUrl
            }
            this.postImage(bodyimage)
        })
        }
  render() {
    
    const { previewVisible, previewImage, fileList, previewTitle, respone } = this.state;
    console.log("hung", respone);

    const uploadButton = (
      <div>
        <PlusOutlined />
        <div style={{ marginTop: 8 }}>Upload</div>
      </div>
    );

      const list =[]
      Object.keys(respone).forEach((key) => {
          list.push(<span>{key}:{respone[key]}</span>)
      })
    console.log(list);
    return (
      <>
        <Upload
          action=""
          listType="picture-card"
          fileList={fileList}
          onPreview={this.handlePreview}
          onChange={this.handleChange}
        >
          {fileList.length >= 8 ? null : uploadButton}
        </Upload>
        <Modal
          visible={previewVisible}
          title={previewTitle}
          onCancel={this.handleCancel}
        >
          <img alt="example" style={{ width: '100%' }} src={previewImage} />
        </Modal>
            <div>
                <input type="submit" value="Submit" onClick={(e) => this.submitImage(fileList)}/>
            </div>
        <div>{list}</div>
        </>
    );
  }
}

export default CMT;
