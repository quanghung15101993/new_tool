import {Button, Form, Input, Modal, Select} from "antd";
import React, {useEffect, useRef, useState} from "react";
import {openNotificationCommon, REST_API_PROJECT_ALL, REST_API_REG_PROFILE_CREATE} from "../../common/const";
import RecogProfile from "./recogProfile"

function RecogProfileForm() {
    const [listProjectSource, setListProjectSource] = useState([]);
    const [loading, setLoading] = useState(false)
    const [form] = Form.useForm();
    const {Option} = Select;

    const [isModalVisible, setIsModalVisible] = useState(false);
    const showModal = () => {
        setIsModalVisible(true);
    };
    const handleCancel = () => {
        setIsModalVisible(false);
    };

    //get list project id
    const getDataProject = async (params = {}) => {
        setLoading(true);
        let url = REST_API_PROJECT_ALL;
        fetch(url, {
            method: "GET",
            headers: {
                "Content-Type": "application/vnd.api+json",
            },
        })
            .then((response) => response.json())
            .then((repos) => {
                console.log(repos);
                let data = repos.data;
                let list = [];
                for (let i = 0; i < data.length; i++) {
                    list.push(<Option key={data[i].id} value={data[i].id}>{data[i].attributes.name}</Option>);
                }
                console.log("list project id " + list)
                setListProjectSource(list);
                setLoading(false);
            })
            .catch((err) => console.log(err));
    };
    // Insert
    const insertData = async (params = {}) => {
        setLoading(true);
        let url = REST_API_REG_PROFILE_CREATE;
        let data = JSON.stringify({
            "data": {
                "type": "RecogProfileApiView",
                "attributes": {"name": params.name, "project_id": params.projectId}
            }
        });
        fetch(url, {
            method: "POST",
            headers: {
                "Content-Type": "application/vnd.api+json",
            },
            body: data,
        })
            .then((response) => response.json())
            .then((repos) => {
                console.log(repos);
                setLoading(false);
                openNotificationCommon('success', 'Thêm bản ghi', 'Thêm mới thành công bản ghi');
                handleCancel()
            })
            .catch((err) => console.log(err));
    };
    // get list project ID
    useEffect(() => {
        getDataProject({});
    }, [])
    // reset form fields when modal is form, closed
    const useResetFormOnCloseModal = ({form, visible}) => {
        const prevVisibleRef = useRef();
        useEffect(() => {
            prevVisibleRef.current = visible;
        }, [visible]);
        const prevVisible = prevVisibleRef.current;
        useEffect(() => {
            if (!visible && prevVisible) {
                form.resetFields();
            }
        }, [visible]);
    };

    const onOk = () => {
        form.submit();
    };

    const onFinish = (values) => {
        insertData(values);
    };

    return (
        <>
            <Button type="primary" onClick={showModal}>
                Thêm mới recog profile
            </Button>

            <Modal title="Thêm mới"
                   visible={isModalVisible}
                   onOk={onOk}
                   onCancel={handleCancel}
                   footer={[
                       <Button key="submit" type="primary" loading={loading} onClick={onOk}>
                           Thêm mới
                       </Button>,
                       <Button key="back" danger onClick={handleCancel}>
                           Hủy
                       </Button>,
                   ]}>
                <Form form={form} layout="vertical" name="userForm" onFinish={onFinish}>
                    <Form.Item
                        name="name"
                        label="Recognition Profile Name:"
                        rules={[{required: true, message: 'Please input Recognition Profile Name!'}]}>
                        <Input maxLength={45}/>
                    </Form.Item>

                    <Form.Item
                        name="projectId"
                        label="Project:"
                        rules={[{required: true, message: 'Please choose your Project!'}]}>
                        <Select
                            placeholder="Select a option and change input text above"
                            allowClear>
                            {listProjectSource}
                        </Select>
                    </Form.Item>
                </Form>
            </Modal>
        </>
    );
}

export default RecogProfileForm;

