import React, {useEffect, useRef, useState} from "react";
import "antd/dist/antd.css";
import {openNotificationCommon, REST_API_REG_PROFILE, REST_API_REG_PROFILE_REMOVE} from "../../common/const";
import Space from "antd/lib/space";
import {Popconfirm, message, Button, Modal, notification, Table, Form, Input, Checkbox, Select} from 'antd';
import RecogProfileForm from "./recogProfileForm"

function RecogProfile() {
    const [dataSource, setDataSource] = useState([]);
    const [loading, setLoading] = useState(false)
    const [total, setTotal] = useState(10)
    const [current, setCurrent] = useState(1)
    const [pagination, setPagination] = useState({current: 1, pageSize: 10,});
    const {confirm} = Modal;

    const columns = [
        {
            title: "Name",
            dataIndex: "name",
            key: "name",
            sorter: true,
            width: '40%',
        },
        {
            title: "Project ID",
            dataIndex: "projectid",
            key: "projectid",
            sorter: true,
        },
        {
            title: 'Action',
            key: 'action',
            fixed: 'right',
            width: '20%',
            render: (text, record) => (
                <Space>
                    <Button type="primary" key="record.add">
                        Sửa
                    </Button>
                    <Button danger onClick={() => removeColumn(record.id)} key="record.remove">
                        Xóa
                    </Button>
                </Space>
            ),

        },
    ];


    const getRecogProfile = async (params = {}) => {
        setLoading(true);
        let url = REST_API_REG_PROFILE
        if (params.page) {
            url = REST_API_REG_PROFILE + "?page=" + params.page;
            setCurrent(params.page);
        }else{
            setCurrent(1);
        }

        fetch(url, {
            method: "GET",
            headers: {
                "Content-Type": "application/vnd.api+json",
            },
        })
            .then((response) => response.json())
            .then((repos) => {
                console.log(repos);
                let data = repos.data;
                let list = [];
                let count = repos.meta.pagination.count;
                for (let i = 0; i < data.length; i++) {
                    list.push({
                        id: data[i].id,
                        name: data[i].attributes.name,
                        projectid: data[i].attributes.project_id,
                    });
                }
                setDataSource(list);
                setLoading(false)
                setTotal(count)
            })
            .catch((err) => console.log(err));
    };

    //useEffect
    useEffect(() => {
        getRecogProfile({});
    }, []);

    const handleTableChange = (pagination, filters, sorter) => {
        getRecogProfile({
            page: pagination.current
        });
    };

    const removeColumn = (id) => {
        confirm({
            title: 'Are you sure delete this task?',
            content: 'Some descriptions',
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk() {
                let url = REST_API_REG_PROFILE_REMOVE + "/" + id;
                fetch(url, {
                    method: "DELETE",
                    headers: {
                        "Content-Type": "application/vnd.api+json"
                    },
                })
                    .then((response) => response.json())
                    .then((repos) => {
                        getRecogProfile();
                        setPagination(1);
                        openNotificationCommon('success', 'Xóa bản ghi', 'Xóa thành công bản ghi');
                    })
                    .catch((err) => {
                        console.log(err);
                        openNotificationCommon('error', 'Xóa bản ghi', 'Xóa không thành công bản ghi')
                    });
            },
            onCancel() {
            },
        });
    };

    return (
        <>
            <RecogProfileForm/>
            <Table columns={columns}
                   dataSource={dataSource}
                   loading={loading}
                   pagination={{total: total,current: current}}
                   onChange={handleTableChange}
            />
        </>

    );
}

export default RecogProfile;
