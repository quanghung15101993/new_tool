import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getAll } from "../../store/actions/projectAction"
import {
    EditTwoTone,
    DeleteTwoTone,
} from '@ant-design/icons';
import {Table} from "antd";

const columns = [
        {
            title: "Name",
            dataIndex: "name",
            key: "name",
        },
        {
            title: "Project ID",
            dataIndex: "projectid",
            key: "projectid",
        },
        {
            title: 'Action',
            key: 'action',
            fixed: 'right',
            width: 300,
            render: () => (
                <div size="middle">
                    <button type="button"><EditTwoTone /></button>
                    <button type="button"><DeleteTwoTone /></button>
                </div>
            ),
        },
];
    
const Project = ({ getAll, projectData }) => {
    const [dataSource, setDataSource] = useState([]);

    /*Push action getAll to projectAction
    The useEffect is similar to componentDidMount and componentDidUpdate:*/
    useEffect(() => {
        getAll()
    }, [])



console.log("get state", projectData)
    return (
        
        <div>
            <Table columns={columns}
                dataSource={projectData}
            />
        </div>
    );
}

//Get state from Store
const mapStateToProps = (state) => ({
  projectData: state.myProject.project,
});
//Connect to store
export default connect(mapStateToProps, { getAll })(Project);
