import Project from "../components/project/project";
import RecogProfile from "../components/recog-profile/recogProfile";
import {notification} from "antd";
import CMT from "../components/cmt/cmt";
/*const routes menu*/
export const routes = [
    {
        path: "/project",
        component: Project,
        breadcrumb: "project",
    },
    ,
    {
        path: "/ip-project",
        component: RecogProfile,
        breadcrumb: "ip-project",
    },
    {
        path: "/ip-flow",
        component: RecogProfile,
        breadcrumb: "ip-flow",
    },
    {
        path: "/recog-profile",
        component: RecogProfile,
        breadcrumb: "recog-profile",
    },
    {
        path: "/templates",
        component: RecogProfile,
        breadcrumb: "templates",
    },
    {
        path: "/extraction",
        component: RecogProfile,
        breadcrumb: "extraction",
    },
    {
        path: "/testcase",
        component: RecogProfile,
        breadcrumb: "testcase",
    },
    {
        path: "/cmt",
        component: CMT,
        breadcrumb: "cmt",
    }
];
/*const routes menu*/
const domain = "http://127.0.0.1:8000";
export const REST_API_REG_PROFILE = domain + "/rest/cap/reg-profile"
export const REST_API_REG_PROFILE_CREATE = domain + "/rest/cap/reg-profile"
export const REST_API_REG_PROFILE_DETAIL = domain + "/rest/cap/reg-profile"
export const REST_API_REG_PROFILE_REMOVE = domain + "/rest/cap/reg-profile"
export const REST_API_PROJECT_ALL = domain+ "/rest/cap/projects?size = 1000"


/*const breadcrumbs */
export const constBreadcrumb = [
    {
        page: "project",
        name: "Project",
    },
    {
        page: "ip-project",
        name: "Image Processing Project",
    },
    {
        page: "ip-flow",
        name: "Image Processing Flow",
    },
    {
        page: "recog-profile",
        name: "Recognition Profile",
    },
    {
        page: "templates",
        name: "Template",
    },
    {
        page: "extraction",
        name: "Extraction",
    },
    {
        page: "testcase",
        name: "Test Case",
    },
     {
        page: "cmt",
        name: "CMT",
    },
];
/*const breadcrumbs*/

/*notification common*/
export const openNotificationCommon = (type, message, description) => {
    notification.config({
        placement: 'bottomRight',
        bottom: 50,
        duration: 3,
    });

    notification[type]({
        message: message,
        description: description,
    });
};
/*notification common*/

