import React from "react";
import Project from "../components/project/project";
import RecogProfile from "../components/recog-profile/recogProfile";
import CMT from "../components/cmt/cmt";


function ContentCommon(props) {
  let compo;
  switch (props.id) {
    case "project":
      compo = <Project />;
      break;
    case "recog-profile":
      compo = <RecogProfile />;
    case "cmt":
      compo = <CMT />;
      break;
    default:
      compo = <Project />;
      break;
  }
  return (
    <div
      className="site-layout-background"
      style={{ padding: 24, minHeight: 360 }}
    >
      {compo}
    </div>
  );
}

export default ContentCommon;
