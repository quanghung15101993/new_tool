import React from 'react';
import { Layout } from 'antd';

const { Footer } = Layout;

function FooterCommon() {

    return (
        <Footer style={{ textAlign: 'center' }}>&copy; {new Date().getFullYear()} Captiva Tools </Footer>
    );
}

export default FooterCommon;
