import LayoutMain from './layoutMain';
import ContentCommon from './content';
import FooterCommon from './footer';
import HeaderCommon from './header';
export { LayoutMain, ContentCommon, FooterCommon, HeaderCommon }