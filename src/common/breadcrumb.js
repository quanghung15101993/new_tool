import React from "react";
import { Breadcrumb } from "antd";
import {constBreadcrumb} from "./const";
function BreadcrumbCommon(props) {
  /* constBreadcrumb is in the ConstBreadcrumb file.
    Target to config Breadcrumb of project*/
  var result = constBreadcrumb.map((breadcrumb, i) => {
    if (breadcrumb.page === props.page) {
      return (
        <Breadcrumb style={{ margin: "16px 0" }} key="1">
          <Breadcrumb.Item>App</Breadcrumb.Item>
          <Breadcrumb.Item>{breadcrumb.name}</Breadcrumb.Item>
        </Breadcrumb>
      );
    }
  });
  return <div>{result}</div>;
}

export default BreadcrumbCommon;
