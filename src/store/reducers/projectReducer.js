import { GET_ALL_PROJECT } from "../types";

const initialState = {
  project: [
    {
    },
  ],
};

const projectReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_PROJECT:
      console.log("reducer",action.payload);
      return {
        ...state,
        project: action.payload,
      };

    default:
      return state;
  }
};

export default projectReducer;
