import { combineReducers } from "redux";
import projectReducer from "./projectReducer";

const rootReducer = combineReducers({
  myProject: projectReducer,
});

export default rootReducer;
